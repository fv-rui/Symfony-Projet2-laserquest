<?php
/**
 * Created by PhpStorm.
 * User: hello
 * Date: 17/11/2017
 * Time: 11:05
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class CreateUserAdminCommandTest extends KernelTestCase
{
    public function testExecute()
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $application->add(new CreateUserAdminCommand());

        $command = $application->find('app:create:user');
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            array(
                'command'  => $command->getName(),
                'usernameArg' => 'test',
                'emailArg' => 'test@test.com',
                'passwordArg' => 'test',
            ),
            array(
                'interactive' => false,
            )
        );

        $test = '[OK] (Username: test) - (email: test@test.com) has been added successfully.';

        $output = $commandTester->getDisplay();
        $this->assertContains($test, $output);
    }
}