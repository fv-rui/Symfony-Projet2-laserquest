<?php
/**
 * Created by PhpStorm.
 * User: hello
 * Date: 14/11/2017
 * Time: 11:53
 */

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\FidelityCard;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class FidelityCardTest extends TestCase
{
    public function testFidelityCardCanBeCreated()
    {
        $this->assertInstanceOf(
            FidelityCard::class,
            new FidelityCard()
        );
    }

    public function testFidelityCardHasCodeCentre()
    {
        $fidelityCard = new FidelityCard();
        $fidelityCard->setCodeCenter("123");

        $this->assertEquals('123', $fidelityCard->getCodeCenter());
    }

    public function testFidelityCardHasCodeCarte()
    {
        $fidelityCard = new FidelityCard();
        $fidelityCard->setCodeCard("142121");

        $this->assertEquals('142121', $fidelityCard->getCodeCard());
    }

    public function testFidelityCardHasChecksum()
    {
        $fidelityCard = new FidelityCard();
        $fidelityCard->setCodeCenter("123");
        $fidelityCard->setCodeCard("142121");
        $fidelityCard->setChecksum(8);

        $this->assertEquals(8, $fidelityCard->getChecksum());
    }

    public function testFidelityCardHasChecksumNotValid()
    {
        $this->expectException(\ErrorException::class);
        $fidelityCard = new FidelityCard();
        $fidelityCard->setCodeCenter("123");
        $fidelityCard->setCodeCard("142121");
        $fidelityCard->setChecksum(7);

    }
}
