<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class UserTest extends TestCase
{
    public function testUserCanBeCreated()
    {
        $this->assertInstanceOf(
            User::class,
            new User()
        );
    }

    public function testUserHasNotValidEmailAddress()
    {
            $this->expectException(\ErrorException::class);
            $user = new User();
            $user->setMail('invalidmail');
    }

    public function testUserHasValidEmailAddress()
    {
        $user = new User();
        $user->setMail('validmail@gmail.com');
        $this->assertEquals('validmail@gmail.com',$user->getMail());
    }
}
