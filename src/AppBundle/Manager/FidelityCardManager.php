<?php

namespace AppBundle\Manager;

use AppBundle\Entity\FidelityCard;
use AppBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Skies\QRcodeBundle\Generator\Generator;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class FidelityCardManager
{
    private $objectManager;
    private $session;

    public function __construct(ObjectManager $objectManager, SessionInterface $session)
    {
        $this->objectManager = $objectManager;
        $this->session = $session;
    }

    protected function getRepository()
    {
        return $this->objectManager->getRepository(FidelityCard::class);
    }

    public function calculChecksun(FidelityCard $fidelityCard)
    {
        $arr1 = str_split($fidelityCard->getCodeCard());
        $arr2 = str_split($fidelityCard->getCodeCenter());
        $result = array_merge($arr1, $arr2);
        return array_sum($result) % 9;
    }
    /**
     * @param $level
     * @param $message
     */
    protected function displayFlash($level, $message)
    {
        $this->session->getFlashBag()->add($level, $message);
    }

    /**
     * @return string
     */
    public function generateCodeCard()
    {
        $lenghtMax = 6;
        $arrayCodeCard = [];
        for ($i=0 ; $i < $lenghtMax; $i++) {
            $arrayCodeCard[] = mt_rand(0, 9);
        }
        return implode($arrayCodeCard);
    }

    /**
     * @param FidelityCard $fidelityCard
     * @return bool
     */
    public function createFidelityCard(FidelityCard $fidelityCard)
    {
        $exist = $this->getRepository()->findBy(
            array(
                'codeCenter' => $fidelityCard->getCodeCenter(),
                'codeCard'=>$fidelityCard->getCodeCard()
            )
        );

        if ($exist) {
            $this->displayFlash('danger', 'Cette carte existe déjà !');
            return false;
        }

        $fidelityCard->setChecksum($this->calculChecksun($fidelityCard));

        $this->objectManager->persist($fidelityCard);
        $this->objectManager->flush();
        $this->displayFlash('success', 'La carte a bien été crée !');
        return true;
    }

    public function updateFidelityCard(FidelityCard $fidelityCard)
    {
        $testExist = $this->getRepository()->findBy(
            array(
                'codeCenter' => $fidelityCard->getCodeCenter(),
                'codeCard'=>$fidelityCard->getCodeCard()
            )
        );

        if (($testExist) && ($testExist[0]->getId() !== $fidelityCard->getId())) {
            $this->displayFlash('danger', 'Cette combinaison de code centre et code carte existe déjà, la modification ne peut être éffectué !');
        } else {
            $fidelityCard->setChecksum($this->calculChecksun($fidelityCard));
            $this->objectManager->flush();
            $this->displayFlash('success', 'La carte a bien été modifié !');
        }
    }

    public function searcheFidelityCard(FidelityCard $fidelityCard)
    {
        if ($fidelityCard->getChecksum() === $this->calculChecksun($fidelityCard)) {
            $exist = $this->getRepository()->findOneBy(
                array(
                    'codeCenter' => $fidelityCard->getCodeCenter(),
                    'codeCard'=>$fidelityCard->getCodeCard(),
                    'checksum'=>$fidelityCard->getChecksum()
                )
            );
            if ($exist) {
                return $exist;
            } else {
                $this->displayFlash('danger', "Cette carte n'existe pas !");
                return false;
            }
        } else {
            $this->displayFlash('danger', "Code carte invalid !");
            return false;
        }
    }
    public function QRcode(FidelityCard $fidelityCard)
    {
        $options = array(
            'code'   => $fidelityCard->getCodeCenter().'-'.$fidelityCard->getCodeCard().'-'.$fidelityCard->getChecksum(),
            'type'   => 'qrcode',
            'format' => 'png',
            'width' => 5,
            'height' => 5,
            'color' => [0, 0, 0],
        );

        $generator = new Generator();

        return $generator->generate($options);
    }

    public function linkFidelityCard(FidelityCard $fidelityCard, User $user)
    {
        $fidelityCard->setUser($user);
        $fidelityCard->setStatus("activate");
        $user->addFidelityCards($fidelityCard);

        $this->objectManager->persist($fidelityCard);
        $this->objectManager->persist($user);
        $this->objectManager->flush();

        $this->displayFlash('success', "La carte a bien été enregistré !");
    }
}
