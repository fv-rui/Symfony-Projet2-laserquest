<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * FidelityCardRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class FidelityCardRepository extends EntityRepository
{
}
