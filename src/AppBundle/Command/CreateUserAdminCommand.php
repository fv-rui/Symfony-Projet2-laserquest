<?php
/**
 * Created by PhpStorm.
 * User: hello
 * Date: 17/11/2017
 * Time: 11:32
 */

namespace AppBundle\Command;

use AppBundle\Entity\User;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class CreateUserAdminCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:create:user')
            ->setDescription('Create a new user admin.')
            ->setHelp('This command allows you to create a user admin...')
            ->addArgument('usernameArg', InputArgument::OPTIONAL, 'The username of the user.')
            ->addArgument('emailArg', InputArgument::OPTIONAL, 'The email of the user.')
            ->addArgument('passwordArg', InputArgument::OPTIONAL, 'The password of the user.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Create a user');
        $user = new User();

        if ($input->getArgument('usernameArg')!== null) {
            $usernameArg = $input->getArgument('usernameArg');
        } else {
            $usernameArg = null;
        }

        if ($input->getArgument('emailArg')!== null) {
            $emailArg = $input->getArgument('emailArg');
        } else {
            $emailArg = null;
        }

        if ($input->getArgument('passwordArg')!== null) {
            $passwordArg = $input->getArgument('passwordArg');
        } else {
            $passwordArg = null;
        }

        $username = $io->ask('Please enter username', $usernameArg, function ($username) use ($user) {
            $user->setUsername($username);
            return $this->validate($user, 'username', $username);
        });


        $email = $io->ask('Please enter email', $emailArg, function ($email) use ($user) {
            $user->setMail($email);
            return $this->validate($user, 'mail', $email);
        });

        $password = $io->ask('Please enter Password', $passwordArg, function ($password) use ($user) {
            $user->setPassword($password);
            return $this->validate($user, 'password', $password);
        });

        $user->setRoles(['ROLE_ADMIN']);

        /***** for test ***/
        if ($user->getUsername() == null) {
            $user->setUsername($usernameArg);
        }
        if ($user->getMail() == null) {
            $user->setMail($emailArg);
        }
        if ($user->getPassword() == null) {
            $user->setPassword($passwordArg);
        }

        $encodedPassword = $this->getContainer()->get('security.password_encoder')->encodePassword($user, $user->getPassword());
        $user->setPassword($encodedPassword);

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $em->persist($user);
        $em->flush();

        $io->success('(Username: '.$user->getUsername().') - (email: '.$user->getMail().') has been added successfully.');

        // on supprime l'enregeristrement effcetuer par le test unitaire
        if (($usernameArg === "test") && ($emailArg === "test@test.com") && ($passwordArg === "test")) {
            $em->remove($user);
            $em->flush();
        }
    }

    private function validate($object, $field, $value)
    {
        $validator = $this->getContainer()->get('validator');
        $validation = $validator->validateProperty($object, $field);
        if (0 !== count($validation)) {
            throw new \RuntimeException($validation->get(0)->getMessage());
        }

        return $value;
    }
}
