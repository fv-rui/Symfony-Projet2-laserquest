<?php
/**
 * Created by PhpStorm.
 * User: hello
 * Date: 30/11/2017
 * Time: 10:01
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixturesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:fixtures')
            ->setDescription('init fixtures.')
            ->setHelp('This command to drop table -> update -> fixtures')
        ;
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**********************************************************************
         *************************** drop database
         */
        $command = $this->getApplication()->find('doctrine:database:drop');

        $arguments = array(
            'command' => 'doctrine:database:drop',
            '--force'  => true,
        );

        $greetInput = new ArrayInput($arguments);
        $returnCode = $command->run($greetInput, $output);
        $output->writeln($returnCode);

        /**********************************************************************
         *************************** create database
         */
        $command = $this->getApplication()->find('doctrine:database:create');

        $arguments = array(
            'command' => 'doctrine:database:create',
        );

        $greetInput = new ArrayInput($arguments);
        $returnCode = $command->run($greetInput, $output);
        $output->writeln($returnCode);

        /*****************************************************************
         **************************** update database
         */
        $command = $this->getApplication()->find('doctrine:schema:update');

        $arguments = array(
            'command' => 'doctrine:schema:update',
            '--force'  => true,
        );

        $greetInput = new ArrayInput($arguments);
        $returnCode = $command->run($greetInput, $output);
        $output->writeln($returnCode);

        /*****************************************************************
         **************************** fixtures load
         */
        $command = $this->getApplication()->find('doctrine:fixtures:load');

        $arguments = array(
            'command' => 'doctrine:fixtures:load',
        );

        $greetInput = new ArrayInput($arguments);
        $returnCode = $command->run($greetInput, $output);
        $output->writeln($returnCode);
    }
}
