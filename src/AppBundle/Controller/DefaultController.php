<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/registration", name="registration")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();
        $form = $this->createForm('AppBundle\Form\UserType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $encodedPassword = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($encodedPassword);
            $user->setRoles(['ROLE_USER']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }
        return $this->render('default/registration.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * Lists all center entities.
     *
     * @Route("/ours_centers", name="ours_centers")
     * @Method("GET")
     */
    public function oursCentersIndexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $centers = $em->getRepository('AppBundle:Center')->findAll();

        return $this->render('default/ours_centers.html.twig', array(
            'centers' => $centers,
        ));
    }

    /**
     * ranking global
     *
     * @Route("/ranking", name="ranking")
     * @Method("GET")
     */
    public function rankingAction()
    {
        $objectManager = $this->getDoctrine()->getManager();

        $repo = $objectManager->getRepository('AppBundle:Player');

        $stats = $repo->rankingGlobal();

        return $this->render('default/ranking_global.html.twig', array(
            'stats' => $stats,
        ));
    }

    /**
     * games by user
     *
     * @Route("/my_games", name="my_games")
     * @Method("GET")
     */
    public function my_gamesAction()
    {
        $objectManager = $this->getDoctrine()->getManager();

        $stats = $objectManager->getRepository('AppBundle:Player')->findBy(array('user' => $this->getUser()->getId()));

        return $this->render('default/my_games.html.twig', array(
            'stats' => $stats,
        ));
    }

    /**
     * stats
     *
     * @Route("/stats", name="stats")
     * @Method("GET")
     */
    public function statsAction()
    {
        $objectManager = $this->getDoctrine()->getManager();

        $repo = $objectManager->getRepository('AppBundle:Player');

        $tabScores = $repo->bestAndTotalScore($this->getUser()->getId());
        $nbGames = $repo->nbGames($this->getUser()->getId());
        $gamesBestScore = $repo->findBy(array('user' => $this->getUser()->getId(),'score'=>$tabScores['bestScore']));

        if ($nbGames != 0) {
            $tabScores['scoreM'] = (int)ceil($tabScores['totalScore']/$nbGames);
        }

        return $this->render('default/stats.html.twig', array(
            'tabScores' => $tabScores,
            'nbGames' => $nbGames,
            'gamesBestScore' => $gamesBestScore,
        ));
    }
}
