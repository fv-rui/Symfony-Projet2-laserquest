<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Game;
use AppBundle\Entity\Player;
use AppBundle\Entity\User;
use ArrayObject;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Game controller.
 *
 * @Route("admin/game")
 */
class GameController extends Controller
{
    /**
     * Lists all game entities.
     *
     * @Route("/home/{page}", defaults={"page" = 1}, name="game_index")
     * @Method("GET")
     * @param $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($page)
    {
        $nb_row_by_page = 10;

        $em = $this->getDoctrine()->getManager();
        $arrayRequest = [];

        $arrayRequest['created']['name'] = 'Created';
        $arrayRequest['created']['result'] = $em->getRepository('AppBundle:Game')->findBy(['status'=>'created']);

        $arrayRequest['addplayers']['name'] = 'Add Players';
        $arrayRequest['addplayers']['result'] = $em->getRepository('AppBundle:Game')->findBy(['status'=>'addplayers']);

        $arrayRequest['valid']['name'] = 'Valid';
        $arrayRequest['valid']['result'] = $em->getRepository('AppBundle:Game')->findBy(['status'=>'valid']);

        $arrayRequest['started']['name'] = 'Started';
        $arrayRequest['started']['result'] = $em->getRepository('AppBundle:Game')->findBy(['status'=>'started']);

        $arrayRequest['finished']['name'] = 'Finished';
        $arrayRequest['finished']['result'] = $em->getRepository('AppBundle:Game')->findBy(['status'=>'finished'], null, $nb_row_by_page, ($page-1)*$nb_row_by_page);

        $allFinished = $em->getRepository('AppBundle:Game')->findBy(['status'=>'finished']);

        return $this->render('game/index.html.twig', array(
            'arrayRequest' => $arrayRequest,
            'nb_row_by_page' => $nb_row_by_page,
            'nbAllFinished' => count($allFinished),
        ));
    }

    /**
     * Creates a new game entity.
     *
     * @Route("/new", name="game_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $nbGames = count($em->getRepository(Game::class)->findAll());
        $game = new Game();

        $game->setName("Game ".($nbGames+1));
        $game->setPlayedAt(new \DateTime());
        $game->setDuration(30);

        $form = $this->createForm('AppBundle\Form\GameType', $game);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $game->setStatus("addplayers");

            $em->persist($game);
            $em->flush();

            return $this->redirectToRoute('game_addplayers', array('id' => $game->getId()));
        }

        return $this->render('game/new.html.twig', array(
            'game' => $game,
            'form' => $form->createView(),
        ));
    }

    /**
     * page for add players for the game
     *
     * @Route("/{id}/addplayers", name="game_addplayers")
     * @Method("GET")
     * @param Game $game
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addPlayersAction(Game $game)
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('AppBundle:User')->findBy([]);

        return $this->render('game/addplayers.html.twig', array(
            'game' => $game,
            'users' => $users,
        ));
    }

    /**
     * player added
     *
     * @Route("/{idgame}/{iduser}/playeradded", name="game_playeradded")
     * @Method("GET")
     * @param Game $idgame
     * @param User $iduser
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function playerAddedAction(Game $idgame, User $iduser)
    {
        $player = new Player();

        $player->setGame($idgame);
        $player->setUser($iduser);
        $player->setUsernameGame($iduser->getUsername());
        $player->setScore(0);

        $idgame->addPlayers($player);

        $em = $this->getDoctrine()->getManager();
        $em->persist($idgame);
        $em->persist($player);

        $em->flush();

        return $this->redirectToRoute('game_addplayers', array('id' => $idgame->getId()));
    }

    /**
     * remove players
     *
     * @Route("/{idgame}/{idplayer}/playerremove", name="game_playerremove")
     * @Method("GET")
     * @param Game $idgame
     * @param Player $idplayer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function playerRemoveAction(Game $idgame, Player $idplayer)
    {
        $idgame->removePlayers($idplayer);

        $objectManager = $this->getDoctrine()->getManager();
        $objectManager->remove($idplayer);
        $objectManager->persist($idgame);

        $objectManager->flush();
        return $this->redirectToRoute('game_addplayers', array('id' => $idgame->getId()));
    }

    /**
     * filter users
     *
     * @Route("/{idgame}/addplayers", name="game_filteruseraction")
     * @Method({"GET", "POST"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function filterUserAction(Game $idgame)
    {
        $search = $_POST['search'];

        if ((strlen($search) == 12) && (substr_count($search, '-') == 2)) {
            $arrayCard = explode('-', $search);
            $repo = $this->getDoctrine()->getManager()->getRepository('AppBundle:FidelityCard');
            $exist = $repo->findOneBy(
                array(
                    'codeCenter' => $arrayCard[0],
                    'codeCard'=> $arrayCard[1],
                    'checksum'=> $arrayCard[2]
                )
            );
            if (null === $exist) {
                $result = null;
            } else {
                $result[] = $exist->getUser();
            }
            //dump("fidelitycard",$exist,$result);
        } else {
            $objectManager = $this->getDoctrine()->getManager();
            $result = $objectManager->getRepository(User::class)->serarchUser($search);
        }

        return $this->render('game/addplayers.html.twig', array(
            'game' => $idgame,
            'users' => $result,
            'searchValue' => $search,
        ));
    }

    /**
     * game as enough players
     *
     * @Route("/{id}/valid", name="game_valid")
     * @Method("GET")
     * @param Game $game
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function validAction(Game $game)
    {
        $game->setStatus("valid");
        $objectManager = $this->getDoctrine()->getManager();

        $objectManager->persist($game);
        $objectManager->flush();

        return $this->redirectToRoute('game_started', array('id'=> $game->getId()));
    }

    /**
     * the game is finished
     *
     * @Route("/{id}/finished", name="game_finished")
     * @Method("GET")
     * @param Game $game
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function finishedAction(Game $game)
    {
        $game->setStatus("finished");
//        dump($game);
//        die;
        $objectManager = $this->getDoctrine()->getManager();

        $objectManager->persist($game);
        $objectManager->flush();

        return $this->redirectToRoute('game_index');
    }

    /**
     * game started
     *
     * @Route("/{id}/started/{initScore}", defaults={"initScore" = 0}, name="game_started")
     * @Method("GET")
     * @param Game $game
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function startedAction(Game $game, $initScore)
    {
        $players = $game->getPlayers();

        $objectManager = $this->getDoctrine()->getManager();

        if (($players->first()->getScore() === 0) and ($initScore === '1')) {
            $arrayPlayer = [];
            foreach ($players as $player) {
                $player->setScore(rand(0, $game->getDuration()));
                $objectManager->persist($player);
            }
            $game->setStatus('started');

            $game->sortPlayers();

            $objectManager->persist($game);
            $objectManager->flush();

            return $this->redirectToRoute('game_started', array('id'=> $game->getId()));
        }

        return $this->render('game/started.html.twig', array(
            'game' => $game,
        ));
    }

    /**
     * Finds and displays a game entity.
     *
     * @Route("/{id}/show", name="game_show")
     * @Method("GET")
     * @param Game $game
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Game $game)
    {
        $deleteForm = $this->createDeleteForm($game);

        return $this->render('game/show.html.twig', array(
            'game' => $game,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing game entity.
     *
     * @Route("/{id}/edit", name="game_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Game $game
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Game $game)
    {
        $deleteForm = $this->createDeleteForm($game);
        $editForm = $this->createForm('AppBundle\Form\GameType', $game);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('game_edit', array('id' => $game->getId()));
        }

        return $this->render('game/edit.html.twig', array(
            'game' => $game,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a game entity.
     *
     * @Route("/{id}/delete", name="game_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Game $game)
    {
        $form = $this->createDeleteForm($game);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($game);
            $em->flush();
        }

        return $this->redirectToRoute('game_index');
    }

    /**
     * Creates a form to delete a game entity.
     *
     * @param Game $game The game entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Game $game)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('game_delete', array('id' => $game->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
