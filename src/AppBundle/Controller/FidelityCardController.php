<?php

namespace AppBundle\Controller;

use AppBundle\Entity\FidelityCard;
use Skies\QRcodeBundle\Generator\Generator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Fidelitycard controller.
 *
 * @Route("admin/fidelitycard")
 */
class FidelityCardController extends Controller
{
    /**
     * Lists all fidelityCard entities.
     *
     * @Route("/", name="fidelitycard_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $fidelityCards = $em->getRepository('AppBundle:FidelityCard')->findAll();

        return $this->render('fidelitycard/index.html.twig', array(
            'fidelityCards' => $fidelityCards,
        ));
    }

    /**
     * Creates a new fidelityCard entity.
     *
     * @Route("/new", name="fidelitycard_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $fidelitycard_manager = $this->get('app.fidelitycard_manager');
        $fidelityCard = new Fidelitycard();
        $fidelityCard->setCodeCard($fidelitycard_manager->generateCodeCard());
        $form = $this->createForm('AppBundle\Form\FidelityCardType', $fidelityCard);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $test = $fidelitycard_manager->createFidelityCard($fidelityCard);
            if (false === $test) {
                return $this->render('fidelitycard/new.html.twig', array(
                    'fidelityCard' => $fidelityCard,
                    'form' => $form->createView(),
                ));
            }
            $em = $this->getDoctrine()->getManager();
            $AllFidelityCards = $em->getRepository('AppBundle:FidelityCard')->findAll();
            return $this->redirectToRoute('fidelitycard_index', array('fidelityCards' => $AllFidelityCards,));
        }
        return $this->render('fidelitycard/new.html.twig', array(
            'fidelityCard' => $fidelityCard,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a fidelityCard entity.
     *
     * @Route("/{id}", name="fidelitycard_show")
     * @Method("GET")
     * @param FidelityCard $fidelityCard
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(FidelityCard $fidelityCard)
    {
        return $this->render('fidelitycard/show.html.twig', array(
            'fidelityCard' => $fidelityCard,
            'qrcode' => $this->get('app.fidelitycard_manager')->QRcode($fidelityCard),
        ));
    }

    /**
     * Displays a form to edit an existing fidelityCard entity.
     *
     * @Route("/{id}/edit", name="fidelitycard_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param FidelityCard $fidelityCard
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, FidelityCard $fidelityCard)
    {
        $editForm = $this->createForm('AppBundle\Form\FidelityCardType', $fidelityCard);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->get('app.fidelitycard_manager')->updateFidelityCard($fidelityCard);
            return $this->redirectToRoute('fidelitycard_edit', array('id' => $fidelityCard->getId()));
        }

        return $this->render('fidelitycard/edit.html.twig', array(
            'fidelityCard' => $fidelityCard,
            'edit_form' => $editForm->createView(),
        ));
    }
}
