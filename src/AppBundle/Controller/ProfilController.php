<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 28/11/2017
 * Time: 12:32
 */

namespace AppBundle\Controller;

use AppBundle\Entity\FidelityCard;
use AppBundle\Entity\InfoUser;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/profil")
 */
class ProfilController extends Controller
{
    /**
     * @Route("/", name="profil")
     */
    public function indexAction(Request $request)
    {
        $fidelityCard = new FidelityCard();
        $formCard = $this->createForm('AppBundle\Form\SearchFidelityCardType', $fidelityCard);

        $user = $this->getUser();
        if ($user->getFidelityCards() !== null) {
            $fidelityCardForQRcode = $user->getFidelityCards()->last();
        }

        $formCard->handleRequest($request);

        if ($formCard->isSubmitted() && $formCard->isValid()) {
            $searchCard = $this->get('app.fidelitycard_manager')->searcheFidelityCard($fidelityCard);

            if (null !== $searchCard->getUser()) {
                $this->addFlash('danger', 'cette carte est déjà attribué !');

                return $this->render('profil/index.html.twig', array(
                    'formCard' => $formCard->createView(),
                ));
            }

            $this->get('app.fidelitycard_manager')->linkFidelityCard($searchCard, $user);
            $fidelityCardForQRcode = $searchCard;
        }

        if ($user->getFidelityCards()->count() > 0) {
            $qrcode = $this->get('app.fidelitycard_manager')->QRcode($fidelityCardForQRcode);
        } else {
            $qrcode = null ;
        }

        return $this->render('profil/index.html.twig', array(
            'formCard' => $formCard->createView(),
            'qrcode' => $qrcode,
        ));
    }

    /**
     * @Route("/edit", name="profil_edit")
     * @Method({"GET", "POST"})
     */
    public function profilEditAction(Request $request)
    {
        $user = $this->getUser();

        $editForm = $this->createForm('AppBundle\Form\UserType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('profil', array('id' => $user->getId()));
        }

        return $this->render('profil/edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),

        ));
    }

    /**
     * @Route("/edit/email", name="profil_edit_email")
     */
    public function profilEditEmailAction(Request $request)
    {
        $user = $this->getUser();

        $editForm = $this->createForm('AppBundle\Form\UserType', $user, [
            'type' => 'email'
        ]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('profil', array('id' => $user->getId()));
        }

        return $this->render('profil/edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),

        ));
    }

    /**
     * @Route("/edit/password", name="profil_edit_password")
     */
    public function profilPasswordAction(Request $request)
    {
        $user = $this->getUser();

        $editForm = $this->createForm('AppBundle\Form\UserType', $user, [
            'type' => 'password'
        ]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('profil', array('id' => $user->getId()));
        }

        return $this->render('profil/edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),

        ));
    }

    /**
     * @Route("/edit/username", name="profil_edit_username")
     */
    public function profilUsernameAction(Request $request)
    {
        $user = $this->getUser();

        $editForm = $this->createForm('AppBundle\Form\UserType', $user, [
            'type' => 'username'
        ]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('profil', array('id' => $user->getId()));
        }

        return $this->render('profil/edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),

        ));
    }
    /**
     * @Route("/edit/infouser", name="profil_edit_infouser")
     * @Method({"GET", "POST"})
     */
    public function editInfoUserAction(Request $request)
    {
        $infoUser = $this->getUser()->getInfosUser();
        $editForm = $this->createForm('AppBundle\Form\InfoUserType', $infoUser);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('profil', array('id' => $infoUser->getId()));
        }
        return $this->render('profil/edit_info_user.html.twig', array(

            'edit_form' => $editForm->createView(),
            'infoUser' => $infoUser
        ));
    }
}
