<?php
/**
 * Created by PhpStorm.
 * User: hello
 * Date: 20/11/2017
 * Time: 12:48
 */

namespace AppBundle\Controller;

use AppBundle\Entity\FidelityCard;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends Controller
{
    /**
     * Lists all carteFidelite entities.
     *
     * @Route("/admin", name="admin_index")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function adminIndexAction(Request $request)
    {
        $fidelitycard_manager = $this->get('app.fidelitycard_manager');
        $fidelityCard = new FidelityCard();
        $form = $this->createForm('AppBundle\Form\SearchFidelityCardType', $fidelityCard);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $search = $fidelitycard_manager->searcheFidelityCard($fidelityCard);
            if (!$search) {
                return $this->render('admin/admin_index.html.twig', array(
                    'form' => $form->createView(),
                ));
            }

            return $this->redirectToRoute('user_show', [
                'id' => $search->getUser()->getId(),

            ]);
        }

        return $this->render('admin/admin_index.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
