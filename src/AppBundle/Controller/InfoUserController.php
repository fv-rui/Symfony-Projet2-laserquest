<?php

namespace AppBundle\Controller;

use AppBundle\Entity\InfoUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Infouser controller.
 *
 * @Route("infouser")
 */
class InfoUserController extends Controller
{
    /**
     * Lists all infoUser entities.
     *
     * @Route("/", name="infouser_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $infoUsers = $em->getRepository('AppBundle:InfoUser')->findAll();

        return $this->render('infouser/index.html.twig', array(
            'infoUsers' => $infoUsers,
        ));
    }

    /**
     * Creates a new infoUser entity.
     *
     * @Route("/new", name="infouser_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $infoUser = new Infouser();
        $form = $this->createForm('AppBundle\Form\InfoUserType', $infoUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($infoUser);
            $em->flush();

            return $this->redirectToRoute('infouser_show', array('id' => $infoUser->getId()));
        }

        return $this->render('infouser/new.html.twig', array(
            'infoUser' => $infoUser,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a infoUser entity.
     *
     * @Route("/{id}", name="infouser_show")
     * @Method("GET")
     */
    public function showAction(InfoUser $infoUser)
    {
        $deleteForm = $this->createDeleteForm($infoUser);

        return $this->render('infouser/show.html.twig', array(
            'infoUser' => $infoUser,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing infoUser entity.
     *
     * @Route("/{id}/edit", name="infouser_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, InfoUser $infoUser)
    {
        $deleteForm = $this->createDeleteForm($infoUser);
        $editForm = $this->createForm('AppBundle\Form\InfoUserType', $infoUser);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('infouser_edit', array('id' => $infoUser->getId()));
        }

        return $this->render('infouser/edit.html.twig', array(
            'infoUser' => $infoUser,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a infoUser entity.
     *
     * @Route("/{id}", name="infouser_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, InfoUser $infoUser)
    {
        $form = $this->createDeleteForm($infoUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($infoUser);
            $em->flush();
        }

        return $this->redirectToRoute('infouser_index');
    }

    /**
     * Creates a form to delete a infoUser entity.
     *
     * @param InfoUser $infoUser The infoUser entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(InfoUser $infoUser)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('infouser_delete', array('id' => $infoUser->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
