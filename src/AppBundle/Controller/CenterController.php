<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Center;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Center controller.
 *
 * @Route("admin/center")
 */
class CenterController extends Controller
{
    /**
     * Lists all center entities.
     *
     * @Route("/", name="center_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $centers = $em->getRepository('AppBundle:Center')->findAll();

        $centersClose = $em->getRepository('AppBundle:Center')->findByStatus('close');
        $centersOpen = $em->getRepository('AppBundle:Center')->findByStatus('open');

        $stats['all'] = count($centers);
        $stats['close'] = count($centersClose);
        $stats['open'] = count($centersOpen);

        return $this->render('center/index.html.twig', array(
            'centers' => $centers,
            'stats' => $stats,
        ));
    }

    /**
     * Creates a new center entity.
     *
     * @Route("/new", name="center_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $center = new Center();

        $form = $this->createForm('AppBundle\Form\CenterType', $center);

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($center);
            $em->flush();

            return $this->redirectToRoute('center_show', array('id' => $center->getId()));
        }

        return $this->render('center/new.html.twig', array(
            'center' => $center,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a center entity.
     *
     * @Route("/{id}", name="center_show")
     * @Method("GET")
     */
    public function showAction(Center $center)
    {
        return $this->render('center/show.html.twig', array(
            'center' => $center,
        ));
    }

    /**
     * Displays a form to edit an existing center entity.
     *
     * @Route("/{id}/edit", name="center_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Center $center)
    {
        $em = $this->getDoctrine()->getManager();
        $centers = $em->getRepository('AppBundle:Center')->findAll();

        $editForm = $this->createForm('AppBundle\Form\CenterType', $center);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('center_edit', array('id' => $center->getId()));
        }

        return $this->render('center/edit.html.twig', array(
            'centerID' => $center,
            'edit_form' => $editForm->createView(),
            'centers' => $centers,
        ));
    }
}
