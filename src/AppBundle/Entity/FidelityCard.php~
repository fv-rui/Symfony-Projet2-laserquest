<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FidelityCard
 *
 * @ORM\Table(name="FidelityCard")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FidelityCardRepository")
 */
class FidelityCard
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codeCenter", type="string", length=3)
     */
    private $codeCenter;

    /**
     * @var string
     *
     * @ORM\Column(name="codeCard", type="string", length=6)
     */
    private $codeCard;

    /**
     * @var integer
     *
     * @ORM\Column(name="checksum", type="integer")
     */
    private $checksum;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="level", type="string", length=255)
     */
    private $level;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Center", inversedBy="fidelityCard")
     */
    private $center;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codeCenter
     *
     * @param string $codeCenter
     *
     * @return FidelityCard
     */
    public function setCodeCenter($codeCenter)
    {
        $this->codeCenter = $codeCenter;

        return $this;
    }

    /**
     * Get codeCenter
     *
     * @return string
     */
    public function getCodeCenter()
    {
        return $this->codeCenter;
    }

    /**
     * Set codeCard
     *
     * @param string $codeCard
     *
     * @return FidelityCard
     */
    public function setCodeCard($codeCard)
    {
        $this->codeCard = $codeCard;

        return $this;
    }

    /**
     * Get codeCard
     *
     * @return string
     */
    public function getCodeCard()
    {
        return $this->codeCard;
    }

    /**
     * Set checksum
     *
     * @param integer $checksum
     *
     * @return FidelityCard
     */
    public function setChecksum($checksum)
    {
        $this->checksum = $this->testChecksun($checksum);

        return $this;
    }

    /**
     * @param $checksun
     * @return mixed
     * @throws \ErrorException
     */
    private function testChecksun($checksun)
    {
        $arr1 = str_split($this->getCodeCard());
        $arr2 = str_split($this->getCodeCenter());
        $result = array_merge($arr1, $arr2);
        $testChacksun = array_sum ($result) % 9;
        if ($testChacksun !== $checksun) {
            throw new \ErrorException("Card number invalid.");
        }
        return $checksun;
    }

    /**
     * Get checksum
     *
     * @return integer
     */
    public function getChecksum()
    {
        return $this->checksum;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return FidelityCard
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set level
     *
     * @param string $level
     *
     * @return FidelityCard
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return FidelityCard
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
