<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="User")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="username", type="string", length=255)
     */
    private $username;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="mail", type="string", length=255)
     */
    private $mail;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var array
     *
     * @ORM\Column(name="roles", type="array")
     */
    private $roles;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\InfoUser", cascade={"persist"})
     */
    private $infosUser;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\FidelityCard", mappedBy="user")
     */
    private $fidelityCards;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Player", mappedBy="user")
     */
    private $games;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->games = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fidelityCards = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return User
     * @throws \ErrorException
     */
    public function setMail($mail)
    {
        if (false === filter_var($mail, FILTER_VALIDATE_EMAIL)) {
            throw new \ErrorException('invalid mail');
        }
        $this->mail = $mail;
        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set roles
     *
     * @param array $roles
     *
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Get roles
     *
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set infosUser
     *
     * @param \AppBundle\Entity\InfoUser $infosUser
     *
     * @return User
     */
    public function setInfosUser(\AppBundle\Entity\InfoUser $infosUser = null)
    {
        $this->infosUser = $infosUser;

        return $this;
    }

    /**
     * Get infosUser
     *
     * @return \AppBundle\Entity\InfoUser
     */
    public function getInfosUser()
    {
        return $this->infosUser;
    }

    /**
     * Add fidelityCard
     *
     * @param \AppBundle\Entity\FidelityCard $fidelityCard
     *
     * @return User
     */
    public function addFidelityCards(\AppBundle\Entity\FidelityCard $fidelityCard)
    {
        $this->fidelityCards[] = $fidelityCard;

        return $this;
    }

    /**
     * Remove fidelityCard
     *
     * @param FidelityCard $fidelityCard
     */
    public function removeFidelityCards(\AppBundle\Entity\FidelityCard $fidelityCard)
    {
        $this->fidelityCards->removeElement($fidelityCard);
    }

    /**
     * Get game
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFidelityCards()
    {
        return $this->fidelityCards;
    }

    /**
     * Add game
     *
     * @param \AppBundle\Entity\Game $game
     *
     * @return User
     */
    public function addGame(\AppBundle\Entity\Game $game)
    {
        $this->games[] = $game;

        return $this;
    }

    /**
     * Remove game
     *
     * @param \AppBundle\Entity\Game $game
     */
    public function removeGame(\AppBundle\Entity\Game $game)
    {
        $this->games->removeElement($game);
    }

    /**
     * Get game
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return false;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
    }
}
