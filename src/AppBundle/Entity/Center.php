<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Center
 *
 * @ORM\Table(name="center")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CenterRepository")
 */
class Center
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="codeCenter", type="string", length=255, unique=true)
     */
    private $codeCenter;

    /**
     * @var int
     *
     * @ORM\Column(name="streetNumber", type="integer")
     */
    private $streetNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255)
     */
    private $street;

    /**
     * @var int
     *
     * @ORM\Column(name="zipCode", type="integer")
     */
    private $zipCode;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="phoneNumber", type="string", length=255)
     */
    private $phoneNumber;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Game", mappedBy="center")
     */
    private $arrGames;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\FidelityCard", mappedBy="center")
     */
    private $fidelityCards;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=6)
     */
    private $status;

    public function __construct()
    {
        $this->arrGames = new ArrayCollection();
        $this->fidelityCards = new ArrayCollection();
        $this->status = "open";
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Center
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCodeCenter()
    {
        return $this->codeCenter;
    }

    /**
     * @param string $codeCenter
     */
    public function setCodeCenter($codeCenter)
    {
        $this->codeCenter = $codeCenter;
    }

    /**
     * Add game
     *
     * @param \AppBundle\Entity\Game $game
     *
     * @return Center
     */
    public function addArrGame(\AppBundle\Entity\Game $game)
    {
        $this->arrGames[] = $game;

        return $this;
    }

    /**
     * Remove game
     *
     * @param \AppBundle\Entity\Game $game
     */
    public function removeArrGame(\AppBundle\Entity\Game $game)
    {
        $this->arrGames->removeElement($game);
    }

    /**
     * Get arrGames
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArrGames()
    {
        return $this->arrGames;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Center
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Add fidelityCard
     *
     * @param \AppBundle\Entity\FidelityCard $fidelityCard
     *
     * @return Center
     */
    public function addFidelityCards(\AppBundle\Entity\FidelityCard $fidelityCard)
    {
        $this->fidelityCards[] = $fidelityCard;

        return $this;
    }

    /**
     * Remove fidelityCard
     *
     * @param \AppBundle\Entity\FidelityCard $fidelityCard
     */
    public function removeFidelityCards(\AppBundle\Entity\FidelityCard $fidelityCard)
    {
        $this->fidelityCards->removeElement($fidelityCard);
    }

    /**
     * Get fidelityCard
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFidelityCards()
    {
        return $this->fidelityCards;
    }

    /**
     * Set streetNumber
     *
     * @param integer $streetNumber
     *
     * @return Center
     */
    public function setStreetNumber($streetNumber)
    {
        $this->streetNumber = $streetNumber;

        return $this;
    }

    /**
     * Get streetNumber
     *
     * @return integer
     */
    public function getStreetNumber()
    {
        return $this->streetNumber;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return Center
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set zipCode
     *
     * @param integer $zipCode
     *
     * @return Center
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return integer
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Center
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return Center
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }
}
