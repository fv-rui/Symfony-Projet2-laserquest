<?php

namespace AppBundle\Form;

use AppBundle\Form\EventListener\EditFormSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['type'] === 'username' || $options['type'] === 'full') {
            $builder->add('username', TextType::class, [
                    'attr' => ['placeholder' => 'username'],
                ]);
        }
        if ($options['type'] === 'email' || $options['type'] === 'full') {
            $builder->add('mail', EmailType::class, [
                    'attr' => ['placeholder' => 'email'],
                ]);
        }
        if ($options['type'] === "password" || $options['type'] === 'full') {
            $builder->add('password', RepeatedType::class, [
                    'type' => PasswordType::class,
                    'first_options' => [
                        'label' => 'Mot de passe',
                        'attr' => ['placeholder' => 'Mot de passe'],
                    ],
                    'second_options' => [
                        'label' => 'Confirmer le mot de passe',
                        'attr' => ['placeholder' => 'Confirmer le mot de passe'],
                    ]
                ]);
        }

//            ->add('roles')
//            ->add('infosUser')
//            ->add('games')
        ;
        $builder->addEventSubscriber(new EditFormSubscriber());
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
            'type' => 'full'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }
}
