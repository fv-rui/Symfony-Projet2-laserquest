<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 06/12/2017
 * Time: 11:12
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class SearchFidelityCardType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('codeCenter', TextType::class)
            ->add('codeCard', TextType::class)
            ->add('checkSum', IntegerType::class)
        ;
    }
}
