<?php

namespace AppBundle\Form;

use AppBundle\Entity\Center;
use AppBundle\Entity\FidelityCard;
use AppBundle\Repository\CenterRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FidelityCardType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('center', EntityType::class, array(
                'class' => Center::class,
                'query_builder' => function (CenterRepository $repo) {
                    return $repo->creatListe();
                },
                'choice_label' => function (Center $centre) {
                    return $centre->getCodeCenter().' - '.$centre->getName();
                },
                'choice_value' => function ($centerValue) {
//                    dump($centerValue);die;
                    return $centerValue ? (string) $centerValue->getCodeCenter() : '';
                },
                'placeholder' => 'Choose an Center',
                'expanded' => false,
                'multiple' => false,


            ))
            ->add('codeCard', TextType::class, array())
//            ->add('checksum')
            ->add('status', ChoiceType::class, array(
                'choices'  => array(
                    'Deactivate' => 'deactivate',
                    'Activate' => 'activate',
                    'Unavailable (stolen or lost)' => 'unavailable',
                    'Order' => 'order',
                    'Available' => 'available',
                ),
                'expanded' => true,
                'multiple' => false,
            ))
            ->add('level', ChoiceType::class, array(
                'choices'  => array(
                    'Standard' => 'standard',
                    'Prenium' => 'prenium',
                ),
                'expanded' => true,
                'multiple' => false,
            ))

//            ->add('user')
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\FidelityCard'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_fidelitycard';
    }

    public function choices($repository)
    {
        return $centers;
    }
}
