<?php

namespace AppBundle\Form;

use AppBundle\Entity\Center;
use AppBundle\Repository\CenterRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GameType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('playedAt', DateTimeType::class)
            ->add(
                'duration',
                ChoiceType::class,
                [
                    'choices'  => array('30 min'=>30,'60 min'=>60),
                    'expanded'  => true,
                    'multiple'  => false,
                ]
            )
            ->add('center', EntityType::class, array(
                'class' => Center::class,
                'query_builder' => function (CenterRepository $repo) {
                    return $repo->creatListe();
                },
                'choice_label' => function (Center $centre) {
                    return $centre->getCodeCenter().' - '.$centre->getName();
                },
                'choice_value' => function ($centerValue) {
                    return $centerValue ? (string) $centerValue->getCodeCenter() : '';
                },
                'placeholder' => 'Choose an Center',
                'expanded' => false,
                'multiple' => false,
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Game'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_game';
    }
}
