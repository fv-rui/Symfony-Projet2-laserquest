<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Center;
use AppBundle\Entity\FidelityCard;
use AppBundle\Entity\Game;
use AppBundle\Entity\InfoUser;
use AppBundle\Entity\Player;
use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Provider\DateTime;
use Faker\Provider\Internet;

class LoadDataFixtures extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $faker->addProvider(new Internet($faker));
        $faker->addProvider(new DateTime($faker));

        /****************************
         ****** creation des centres
         ***************************/
        $arrayNameCenter = array('Paris','Lille','Bourgogne');
        $arrayCenter = [];
        for ($i = 0; $i < 3; $i++) {
            $center = new Center();
            $center->setName('Shinigami Laser '.$arrayNameCenter[$i]);
            $center->setCodeCenter((string)$faker->randomNumber(3, true));
            $center->setStreetNumber($faker->buildingNumber);
            $center->setStreet($faker->streetName);
            if ($i == 0) {
                $center->setZipCode('75019');
            }
            if ($i == 1) {
                $center->setZipCode('59000');
            }
            if ($i == 2) {
                $center->setZipCode('51110');
            }
            $center->setCity($arrayNameCenter[$i]);
            $center->setPhoneNumber($faker->phoneNumber);
            $arrayCenter[] =$center;
            $manager->persist($center);
        }
        $manager->flush();

        /**************************************************************************
         ****** creation de tous les utilisateurs et leur information personnelles
         **************************************************************************/
        $infoUser = new InfoUser();
        $infoUser->setFirstName($faker->firstName);
        $infoUser->setLastName($faker->lastName);
        $infoUser->setStreetNumber($faker->buildingNumber);
        $infoUser->setStreet($faker->streetName);
        $infoUser->setZipCode($faker->bothify('#####'));
        $infoUser->setCity($faker->city);
        $infoUser->setPhoneNumber($faker->phoneNumber);
        $infoUser->setBirthday($faker->dateTimeBetween('-50 years', '-8 years'));
        $infoUser->setAvatar('default_avatar.jpg');

        $arrayUsers = [];

        $manager->persist($infoUser);
        // fixtures role admin
        $user = new User();
        $user->setUsername('admin');
        $user->setMail('admin@shinigamilaser.fr');

        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($user, 'admin');
        $user->setPassword($password);

        $user->setRoles(['ROLE_ADMIN']);
        $user->setInfosUser($infoUser);

        $arrayUsers[] = $user;
        $manager->persist($user);

        /*************************************************************************************************
         ****** fixtures role test
         ************************************************************************************************/
        $infoUser = new InfoUser();
        $infoUser->setFirstName($faker->firstName);
        $infoUser->setLastName($faker->lastName);
        $infoUser->setStreetNumber($faker->buildingNumber);
        $infoUser->setStreet($faker->streetName);
        $infoUser->setZipCode($faker->bothify('#####'));
        $infoUser->setCity($faker->city);
        $infoUser->setPhoneNumber($faker->phoneNumber);
        $infoUser->setBirthday($faker->dateTimeBetween('-50 years', '-8 years'));
        $infoUser->setAvatar('default_avatar.jpg');

        $user = new User();
        $user->setUsername('usertest');
        $user->setMail('usertest@shinigamilaser.fr');

        $password = $encoder->encodePassword($user, 'test');
        $user->setPassword($password);

        $user->setRoles(['ROLE_ADMIN']);
        $user->setInfosUser($infoUser);

        $arrayUsers[] = $user;
        $manager->persist($infoUser);
        $manager->persist($user);

        /*******************************************************************************************
        *****fixtures role staf
        *******************************************************************************************/
        for ($i = 0; $i < 3; $i++) {
            $infoUser = new InfoUser();
            $infoUser->setFirstName($faker->firstName);
            $infoUser->setLastName($faker->lastName);
            $infoUser->setStreetNumber($faker->buildingNumber);
            $infoUser->setStreet($faker->streetName);
            $infoUser->setZipCode($faker->bothify('#####'));
            $infoUser->setCity($faker->city);
            $infoUser->setPhoneNumber($faker->phoneNumber);
            $infoUser->setBirthday($faker->dateTimeBetween('-50 years', '-8 years'));
            $infoUser->setAvatar('default_avatar.jpg');

            $user = new User();
            $user->setUsername('userstaf'.($i+1));
            $user->setMail($faker->email());

            $password = $encoder->encodePassword($user, 'test');
            $user->setPassword($password);

            $user->setRoles(['ROLE_STAF']);
            $user->setInfosUser($infoUser);

            $arrayUsers[] = $user;
            $manager->persist($infoUser);
            $manager->persist($user);
        }

        $manager->flush();

        /******************************
         fixtures role user
         ******************************/
        for ($i = 0; $i < 20; $i++) {
            $infoUser = new InfoUser();
            $infoUser->setFirstName($faker->firstName);
            $infoUser->setLastName($faker->lastName);
            $infoUser->setStreetNumber($faker->buildingNumber);
            $infoUser->setStreet($faker->streetName);
            $infoUser->setZipCode($faker->bothify('#####'));
            $infoUser->setCity($faker->city);
            $infoUser->setPhoneNumber($faker->phoneNumber);
            $infoUser->setBirthday($faker->dateTimeBetween('-50 years', '-8 years'));
            $infoUser->setAvatar('default_avatar.jpg');

            $user = new User();
            $user->setUsername('user'.($i+1));
            $user->setMail($faker->email());

            $password = $encoder->encodePassword($user, 'test');
            $user->setPassword($password);

            $user->setRoles(['ROLE_USER']);
            $user->setInfosUser($infoUser);

            $arrayUsers[] = $user;
            $manager->persist($user);
            $manager->persist($infoUser);
        }
        $manager->flush();

        /***************************************************************************************
        **** creation des cartes de ficelité
        ***************************************************************************************/
        $arrayStatusFidelityCard = array(
//            'deactivate',
            'activate',
            'unavailable',
//            'order',
//            'available',
        );

        $fidelitycard_manager = $this->container->get('app.fidelitycard_manager');

        for ($i = 0; $i < count($arrayUsers); $i++) {
            $fidelityCard = new FidelityCard();
            $fidelityCard->setCodeCard($fidelitycard_manager->generateCodeCard());
            $centerFC = $faker->randomElement($arrayCenter);
            $fidelityCard->setCenter($centerFC);
            $fidelityCard->setStatus($faker->randomElement($arrayStatusFidelityCard));
            $fidelityCard->setChecksum($fidelitycard_manager->calculChecksun($fidelityCard));
            $fidelityCard->setUser($arrayUsers[$i]);

            $arrayUsers[$i]->addFidelityCards($fidelityCard);
            $centerFC->addFidelityCards($fidelityCard);

            $manager->persist($fidelityCard);
            $manager->persist($centerFC);
            $manager->persist($arrayUsers[$i]);
        }
        $manager->flush();

        $nbGames = 5;

        for ($i = 0 ; $i < $nbGames ; $i++) {
            $nbPlayers = rand(5, 10);
            $centerInGame = $faker->randomElement($arrayCenter);
            $game = new Game();
            $game->setCenter($centerInGame);
            $game->setPlayedAt($faker->dateTimeBetween('-'.$nbGames.'years', 'now'));
            $game->setDuration($faker->randomElement([30,60]));
            $game->setName("Game ".($i+1));
            $game->setStatus("finished");

            $manager->persist($game);

            $trouver = false;
            for ($ip = 0 ; $ip < $nbPlayers ; $ip++) {
                if ($trouver !== true) {
                    $player = new Player();
                }

                $player->setGame($game);
                $player->setUser($faker->randomElement($arrayUsers));
                $player->setUsernameGame($faker->randomElement(['New-'.$player->getUser()->getUsername(),$player->getUser()->getUsername()]));
                $player->setScore(rand(0, $game->getDuration()));

                $playersInGame = $game->getPlayers();

                if ($playersInGame->count() == 0) {
                    $game->addPlayers($player);
                } else {
                    for ($ilp=0 ; $ilp < $playersInGame->count() ; $ilp++) {
                        if ($playersInGame->get($ilp)->getUser()->getId() == $player->getUser()->getId()) {
                            $trouver = true;
                            $ip--;
                            $ilp = $playersInGame->count();
                        } else {
                            $trouver = false;
                        }
                    }
                    if ($trouver !== true) {
                        $game->addPlayers($player);
                    }
                }
                $manager->persist($player);
            }
            $manager->persist($game);
            $centerInGame->addArrGame($game);
            $manager->persist($centerInGame);
        }
        $manager->flush();
    }
}
